﻿#include "pch.h"
#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
	// 検索する可変長配列
	std::vector<int> list = { 3, 13, 8, 9, 5 };

	// 検索する可変長配列をソートします。
	// binary_saerchを行う可変長配列はソートされている必要があります。
	sort(list.begin(), list.end());

	// "4"が可変長配列に含まれるか検索します。
	if (std::binary_search(list.begin(), list.end(), 4))
	{
		std::cout << "検索結果:有" << std::endl;
	}
	else
	{
		std::cout << "検索結果:無" << std::endl; // こちらが表示されます。
	}

	// "13"が可変長配列に含まれるか検索します。
	if (std::binary_search(list.begin(), list.end(), 13))
	{
		std::cout << "検索結果:有" << std::endl; // こちらが表示されます。
	}
	else
	{
		std::cout << "検索結果:無" << std::endl;
	}

	getchar();
}
